# Development best practices

## Branching model


## Version numbering
[Version numbering should follow the 'semantic versioning' model.][1]
Briefly, version numbers should be in the format `vX.Y.Z` where:

* `X` is the major version number.
* `Y` is the minor version number.
* `Z` is the patch version number.

The patch version number is incremented for internal changes (i.e. to build pipelines) or for bug fixes.
The minor version number is incremented when new functionality is added that is backwards compatible.
The major version number is incremented for non backwards compatible API changes that affect the user.

Versions numbers refer to specific commits in the main branch.
These commits should be tagged with the version number.

`git tag vX.Y.Z <commit>`

They may then be pushed to remote.

`git push origin vX.Y.Z`

[1]: https://semver.org/
